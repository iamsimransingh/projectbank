from django.contrib import admin
from myapp.models import register, transactions, notification
# Register your models here.

admin.site.register(register)
admin.site.register(transactions)
admin.site.register(notification)