from django.shortcuts import render

from django.shortcuts import render
from django.http import HttpResponse ,HttpResponseRedirect
from myapp import forms
import datetime
import random
from .forms import registerform,new123,Account123
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.decorators import login_required
from myapp.models import register, User, transactions, notification,contactus

from datetime import date
from dateutil.relativedelta import relativedelta
from django.core import serializers
# Create your views here.
def Homepage(request):
    if request.method=="POST":
        name = request.POST['name']
        em = request.POST['email']
        nb = request.POST['number']
        msz = request.POST['message']

        data = contactus(name=name,email=em,phone=nb,msz=msz)
        data.save()
        return render(request,'index.html',{'status':'Thanks for your Feedback'})
    return render(request,'index.html')

@login_required
def accountstatement(request):
    data = register.objects.filter(user_id=request.user.id)
    data2=register.objects.get(user_id=request.user.id)
    my_temp_balance=(data2.account_balance)
    t = transactions.objects.filter(sender = request.user.username).order_by('-id')|transactions.objects.filter(receiver = request.user.username).order_by('-id')
    return render (request,'accountstatement.html',{'bal':my_temp_balance,'tr':t})
def accountdetails(request):
    data = register.objects.filter(user_id=request.user.id)
    data2=register.objects.get(user_id=request.user.id)
    my_temp_balance=(data2.account_balance)
    t = transactions.objects.filter(sender = request.user.username).order_by('-id')|transactions.objects.filter(receiver = request.user.username).order_by('-id')
    return render (request,'account_details.html',{'d':data,'bal':my_temp_balance,'tr':t})
def profile(request):
    data = register.objects.filter(user_id=request.user.id)
    data2=register.objects.get(user_id=request.user.id)
    my_temp_balance=(data2.account_balance)
    t = transactions.objects.filter(sender = request.user.username).order_by('-id')|transactions.objects.filter(receiver = request.user.username).order_by('-id')
    #mybalance =aman.objects.filter(user_id=request.user.id).update(account_balance)
    today = datetime.date.today()

    third_months = date.today() + relativedelta(months=+3)
    sixth_months=date.today() + relativedelta(months=+6)
    ninth_months=date.today() + relativedelta(months=+9)

    months = ['zero','January','February','March','April','May','June','July','August','September','October','November','December']


    current_month = months[today.month]
    third_month=months[third_months.month]
    six_month=months[sixth_months.month]
    nine_month=months[ninth_months.month]

    third_interest=my_temp_balance+(my_temp_balance/100*3)
    six_interest=third_interest+(my_temp_balance/100*3)
    nine_interest=six_interest+(my_temp_balance/100*3)

    #print(data)
    if request.method == "POST":
        #Deposit_amount =int(request.POST['Deposit_amount'])
        #
        if "Send_money" in request.POST:
            receiver = request.POST['sender_username']
            amount = int(request.POST['sender_amount'])
            password = request.POST['pwd']
            user = User.objects.get(id=request.user.id)
            if user.check_password(password) is True:
                sender_user = register.objects.get(user_id=request.user.id)
                balance = (sender_user.account_balance)

                #aman.objects.filter(user_id=request.user.id).update(account_balance=balance+Deposit_amount)
                #print(balance)
                register.objects.filter(user_id=request.user.id).update(account_balance=balance-amount)
                #print("Update Successfully")
                receiver_user = User.objects.get(username = receiver)
                receiver_data = register.objects.get(user_id=receiver_user.id)
                rec_balance =(receiver_data.account_balance)
                #print(rec_balance)
                register.objects.filter(user_id=receiver_user.id).update(account_balance=rec_balance+amount)
                transac = transactions(sender=sender_user.user.username, receiver = receiver_user.username, amount=amount)
                transac.save()

                message = 'You Send Rs: {}/- to Mr./Miss {}'.format(amount,receiver_data.user.username)
                ###### SEND NOTIFICATION ##########
                notify = notification(user_id=request.user.id, message=message)
                notify.save()

                rcmessage = 'Your Account credited with Rs:{}/-'.format(amount)
                ###### SEND NOTIFICATION ##########
                notify = notification(user_id=receiver_user.id, message=rcmessage)
                notify.save()
            else:
                return render (request,'profile.html',{'d':data,'bal':my_temp_balance,'tr':t,'current_month':current_month,'third_month':third_month,'six_month':six_month,'nine_month':ninth_months,
                "third_interest":third_interest,'six_interest':six_interest,'nine_interest':nine_interest,'al':'Incorrect PIN'})


        elif "Withdraw_money" in request.POST:
            Withdraw_amount =int(request.POST['Withdraw_amount'])
            password = request.POST['pwd']
            user = User.objects.get(id=request.user.id)
            if user.check_password(password) is True:
                sender_user = register.objects.get(user_id=request.user.id)
                balance = (sender_user.account_balance)
                if (balance >= Withdraw_amount):
                    register.objects.filter(user_id=request.user.id).update(account_balance=balance-Withdraw_amount)
                    sufficent_bal="balance Withdrawal Successfully"
                    transac = transactions(sender=None, receiver = user.username, amount=Withdraw_amount)
                    transac.save()

                    message = 'Hello Mr/Miss {} Your Account is debited with Rs: {}/- by you'.format(user.username,Withdraw_amount )

                    ###### SEND NOTIFICATION ##########
                    notify = notification(user_id=request.user.id, message=message)
                    notify.save()
                    

                    return render (request,'profile.html',{'d':data,'al':sufficent_bal,'tr':t,'bal':my_temp_balance})
                else:
                    sufficent_bal="Sorry You Dont Have Sufficent Balance"
                    return render (request,'profile.html',{'d':data,'al':sufficent_bal,'tr':t,'bal':my_temp_balance})
            else:
                return render (request,'profile.html',{'d':data,'bal':my_temp_balance,'tr':t,'current_month':current_month,'third_month':third_month,'six_month':six_month,'nine_month':ninth_months,
                "third_interest":third_interest,'six_interest':six_interest,'nine_interest':nine_interest,'al':'Incorrect PIN'})
        elif "Deposit_money" in request.POST:
            Deposit_amount =int(request.POST['Deposit_amount'])
            password = request.POST['pwd']
            user = User.objects.get(id=request.user.id)
            if user.check_password(password) is True:
                sender_user = register.objects.get(user_id=request.user.id)
                balance = (sender_user.account_balance)
                register.objects.filter(user_id=request.user.id).update(account_balance=balance+Deposit_amount)
                sufficent_bal="balance Deposit Successfully"
                transac = transactions(sender=user.username, receiver = None, amount=Deposit_amount)
                transac.save()

                message = 'Hello Mr/Miss {} Your Account is credited with Rs: {}/-'.format(user.username,Deposit_amount )

                ###### SEND NOTIFICATION ##########
                notify = notification(user_id=request.user.id, message=message)
                notify.save()
                return render (request,'profile.html',{'d':data,'al':sufficent_bal,'tr':t,'bal':my_temp_balance})
            else:
                return render (request,'profile.html',{'d':data,'bal':my_temp_balance,'tr':t,'current_month':current_month,'third_month':third_month,'six_month':six_month,'nine_month':ninth_months,
                "third_interest":third_interest,'six_interest':six_interest,'nine_interest':nine_interest,'al':'Incorrect PIN'})
            # transaction = transactions(sender=sender_user, receiver = receiver_user, amount=amount)
            # transaction.save()

            

    return render (request,'profile.html',{'d':data,'bal':my_temp_balance,'tr':t,'current_month':current_month,'third_month':third_month,'six_month':six_month,'nine_month':ninth_months,
    "third_interest":third_interest,'six_interest':six_interest,'nine_interest':nine_interest})
def SignUP(request):
    form =forms.registerform()
    form1=forms.new123()
    form2=forms.Account123()
    if request.method =='POST':
        form =forms.registerform(data=request.POST)
        form1=forms.new123(request.POST,request.FILES)
        form2=forms.Account123(request.POST)
        if form.is_valid() and form1.is_valid() and form2.is_valid():
            user=form.save()
            user.set_password(user.password)
            user.save()
            profile = form1.save(commit=False)
            profile.user = user
            profile.save()
            if "profile_picture" in request.FILES:
                profile.profile_picture=request.FILES['profile_picture']
            acc=random.randint(11111111,99999999)
            profile.accunt_num = acc
            profile.save()
            account_typ=form2.save(commit=False)
            account_typ.profie = profile
            account_typ.save()
            return render(request,'registeration2.html',{'form':form,'form1':form1,'form2':form2,'status':'Account Created Successfully!! Thanks for registering with Us!!'})
        else:
            print(form.errors, form1.errors,form2.errors)
    return render(request,'registeration2.html',{'form':form,'form1':form1,'form2':form2})

def sign_out(request):
    logout(request)
    return HttpResponseRedirect('/')

def Sign_in(request):
    if request.method == "POST":
        #print("Hello")
        name = request.POST['un']
        pswd = request.POST['pwd']
        #print(name,pswd)
        user = authenticate(username=name, password=pswd)
        if user:
            if user.is_active:
                #print(user)
                login(request,user)
                return HttpResponseRedirect('/profile')
        else:
            wrngpass=("Sorry!! You are not authenticated!!! please check your User Name And Password")
            return render(request,'login.html',{'wrngpass':wrngpass})
    return render(request,'login.html')

def mytrans(request):
    t = transactions.objects.filter(sender = request.user.username).order_by('-id')|transactions.objects.filter(receiver = request.user.username).order_by('-id')
    return render(request,'transaction.html',{'mytr':t})

def recentTrans(request):
    myt1 = transactions.objects.filter(sender = request.user.username).order_by('-id')[:3]
    myt2 = transactions.objects.filter(receiver = request.user.username).order_by('-id')[:3]
    return render(request,'recentTrans.html',{'mytr1':myt1,'mytr2':myt2})




# def form_Fill(request):
#     form =forms.Myform()
#     if request.method =='POST':
#         form =forms.Myform(request.POST)
#
#         if form.is_valid():
#             fname=form.cleaned_data['first_name']
#             lname=form.cleaned_data['last_name']
#             UserEmail=form.cleaned_data['email']
#             UserPass=form.cleaned_data['password']
#             UserAccBal =form.cleaned_data['account_balance']
#             Bank_user.objects.get_or_create(first_name=fname,last_name=lname,email=UserEmail,password=UserPass,account_balance=UserAccBal)[0]
#
#     return render(request,'registeration.html',{'form':form})

def user_notify(request):
    data = notification.objects.filter(user_id=request.user.id, status=False).order_by('-id')
    if 'user' in request.GET:
        total = len(data)
        return HttpResponse(total)
    elif 'show' in request.GET:
        response = serializers.serialize('json',data)
        return HttpResponse(response,content_type='application/json')


def allnotify(request):
    noid = int(request.GET['id'])
    data = notification.objects.get(id=noid)
    data.status=True
    data.save()
    
    notifications = notification.objects.filter(user_id=request.user.id).order_by('-id')
    return render(request,'notifications.html',{'nt':notifications})
