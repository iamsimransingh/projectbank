from django import forms
from myapp.models import register
from django.contrib.auth.models import User

class Myform(forms.Form):
    first_name=forms.CharField()
    last_name=forms.CharField()
    email =forms.EmailField()
    password=forms.CharField(widget=forms.PasswordInput())
    account_balance=forms.IntegerField()

class Account123(forms.ModelForm):
    class Meta():
        model=register
        fields=('title','account_types')
class new123(forms.ModelForm):
    class Meta():
        model=register
        fields =('account_balance','profile_picture')

class registerform(forms.ModelForm):
    password=forms.CharField(widget=forms.PasswordInput())
    confirm_pass=forms.CharField(widget=forms.PasswordInput())
    def clean(self):
        clean_data=super().clean()
        pwd =clean_data['password']
        cnfmpwd=clean_data['confirm_pass']
        if pwd != cnfmpwd:
            raise forms.ValidationError("Password does not match")
    class Meta():
        model=User
        fields =('first_name','last_name','username','email','password')
